### 使用说明
1,本地安装mysql服务,创建一个schema命名成auto-code用于代码生成项目相关信息的存储数据库
2,修改molicode-web中application-dev.properties对应数据库配置
3,使用maven clean compole 编译代码,maven的profile选择dev
4,启动项目,成功后显示登录路径
5,以admin,123456登录系统
6,修改工程目录设置中代码输出根目录,模板根目录,保存
7,修改数据库设置中数据库配置连接待生成代码项目的数据库配置,保存
8,业务代码设置中配置作者(author),基础包路径,artifactId,保存
9,代码生成页面中点击刷新模板列表,选择要生成的模板
10,点击获取Table列表
11,选择要生成代码的表进入配置生成代码进行配置
12,点击执行代码生成生成代码
13,生成代码在6中代码输出根目录中对应位置

