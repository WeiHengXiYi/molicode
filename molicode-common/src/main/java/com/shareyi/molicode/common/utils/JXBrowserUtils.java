package com.shareyi.molicode.common.utils;

import com.teamdev.jxbrowser.chromium.ba;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.math.BigInteger;

/**
 * jx 浏览器工具类
 *
 * @author david
 * @date 2018/8/11
 */
public class JXBrowserUtils {

    /**
     * crack utils
     *
     * @return
     */
    public static boolean crack() {
        try {
            Field e = ba.class.getDeclaredField("e");
            e.setAccessible(true);
            Field f = ba.class.getDeclaredField("f");
            f.setAccessible(true);
            Field modifersField = Field.class.getDeclaredField("modifiers");
            modifersField.setAccessible(true);
            modifersField.setInt(e, e.getModifiers() & ~Modifier.FINAL);
            modifersField.setInt(f, f.getModifiers() & ~Modifier.FINAL);
            e.set(null, new BigInteger("1"));
            f.set(null, new BigInteger("1"));
            modifersField.setAccessible(false);
        } catch (Exception e1) {
            e1.printStackTrace();
            return false;
        }
        return true;
    }
}
