package com.shareyi.molicode.common.vo.code

/**
 * 自定义列配置
 *
 * @author lyz* @since 2021/05/25
 */
class CustomerCol {

    /** 列key **/
    String key;

    /** 列名称 **/
    String name;

    /** 列类型 **/
    String type;

    /** 字段类型 **/
    Object defaultVal;

}
