package com.shareyi.molicode.common.vo.code

/**
 * 自定义表属性
 *
 * @author lyz* @since 2021/05/25
 */
class CustomerTable {

    /** 列key **/
    String key;

    /** 列名称 **/
    String name;

    /** 列类型 **/
    String type;

    /** 字段类型 **/
    Object defaultVal;

}
