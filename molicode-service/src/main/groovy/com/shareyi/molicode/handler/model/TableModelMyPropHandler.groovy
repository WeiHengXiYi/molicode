package com.shareyi.molicode.handler.model

import com.shareyi.molicode.common.chain.handler.SimpleHandler
import com.shareyi.molicode.common.chain.handler.awares.TableModelHandlerAware
import com.shareyi.molicode.common.constants.AutoCodeConstant
import com.shareyi.molicode.common.constants.CommonConstant
import com.shareyi.molicode.common.constants.ConfigKeyConstant
import com.shareyi.molicode.common.enums.DataTypeEnum
import com.shareyi.molicode.common.utils.FileIoUtil
import com.shareyi.molicode.common.utils.SystemFileUtils
import com.shareyi.molicode.common.utils.XmlUtils
import com.shareyi.molicode.common.vo.code.AutoMakeVo
import com.shareyi.molicode.common.vo.code.CustomerCol
import com.shareyi.molicode.common.vo.code.CustomerTable
import com.shareyi.molicode.common.vo.code.TableModelVo
import com.shareyi.molicode.context.TableModelContext
import com.shareyi.molicode.hander.gencode.loader.AutoMakeLoadHandler
import com.shareyi.molicode.service.conf.AcConfigService
import org.springframework.stereotype.Service

import javax.annotation.Resource

/**
 * tableModelVo 从extend.customerCols读取自定义属性信息
 *
 * @author david* @since 2018/10/7
 */
@Service
class TableModelMyPropHandler extends SimpleHandler<TableModelContext> implements TableModelHandlerAware {

    @Resource
    AcConfigService acConfigService;

    @Override
    int getOrder() {
        return 6;
    }

    @Override
    boolean shouldHandle(TableModelContext tableModelContext) {
        return Objects.equals(tableModelContext.tableModelPageVo.modelType, CommonConstant.MODEL_TYPE_JSON) &&
                !tableModelContext.isReadonly();
    }

    @Override
    void doHandle(TableModelContext context) {
        Map<String, Map<String, String>> configMap = acConfigService.getConfigMapByProjectKey(context.tableModelPageVo.projectKey, DataTypeEnum.JSON);
        def pathConfig = configMap.get(ConfigKeyConstant.PathConfig.CONFIG_KEY)
        def templateBaseDir = pathConfig.get(ConfigKeyConstant.PathConfig.TEMPLATE_BASE_PATH)
        String autoXmlPath = SystemFileUtils.parseFilePath(FileIoUtil.contactPath(templateBaseDir, AutoMakeLoadHandler.AUTO_CODE_XML_FILE_NAME));
        AutoMakeVo autoMake = XmlUtils.getAutoMake(autoXmlPath, templateBaseDir)
        String extendCustomerColsPath = autoMake.getProp(AutoCodeConstant.EXTEND_CUSTOMER_COLS_PATH);
        List<CustomerCol> customerCols = XmlUtils.readCustomColFile(FileIoUtil.contactPath(templateBaseDir, extendCustomerColsPath));
        String extendCustomerTablesPath = autoMake.getProp(AutoCodeConstant.EXTEND_CUSTOMER_TABLES_PATH);
        List<CustomerTable> customerTables = XmlUtils.readCustomTableFile(FileIoUtil.contactPath(templateBaseDir, extendCustomerTablesPath));

        TableModelVo tableModelVo = context.tableModelVo;
        tableModelVo.customerCols = customerCols == null ? new ArrayList<CustomerCol>() : customerCols;
        tableModelVo.customerTables = customerTables == null ? new ArrayList<CustomerTable>() : customerTables;
    }

}
