package com.shareyi.molicode.handler.model

import com.shareyi.molicode.common.chain.handler.SimpleHandler
import com.shareyi.molicode.common.chain.handler.awares.TableModelHandlerAware
import com.shareyi.molicode.common.constants.ConfigKeyConstant
import com.shareyi.molicode.common.enums.ResultCodeEnum
import com.shareyi.molicode.common.exception.DefaultExceptionMaker
import com.shareyi.molicode.common.utils.ConfigUtil
import com.shareyi.molicode.common.utils.LogHelper
import com.shareyi.molicode.common.utils.TableNameUtil
import com.shareyi.molicode.common.vo.code.ColumnVo
import com.shareyi.molicode.common.vo.code.TableDefineVo
import com.shareyi.molicode.common.vo.code.TableModelVo
import com.shareyi.molicode.common.vo.page.TableModelPageVo
import com.shareyi.molicode.context.TableModelContext
import groovy.sql.Sql
import org.apache.commons.collections4.MapUtils
import org.apache.commons.lang.StringUtils
import org.springframework.stereotype.Service

import java.sql.DatabaseMetaData

/**
 * tableModel 数据库处理器 - 表信息获取
 *
 * @author david* @since 2018/10/7
 */
@Service
class TableModelDatabaseHandler extends SimpleHandler<TableModelContext> implements TableModelHandlerAware {

    @Override
    int getOrder() {
        return 2;
    }

    @Override
    boolean shouldHandle(TableModelContext tableModelContext) {
        //如果外部传入了tableModel，就不需要查库了，一般是通过SQL解析得来的
        return tableModelContext.tableModelVo == null &&
                !tableModelContext.isReadonly();
    }

    @Override
    void doHandle(TableModelContext tableModelContext) {
        TableModelPageVo tableModelPageVo = tableModelContext.tableModelPageVo;
        Map<String, Map<String, String>> configMap = tableModelContext.getProjectConfigMap();
        Map<String, String> databaseConfigMap = configMap.get(ConfigKeyConstant.DatabaseConfig.CONFIG_KEY);
        String driverName = MapUtils.getString(databaseConfigMap, ConfigKeyConstant.DatabaseConfig.DRIVER_CLASS);
        String url = MapUtils.getString(databaseConfigMap, ConfigKeyConstant.DatabaseConfig.URL);
        String username = MapUtils.getString(databaseConfigMap, ConfigKeyConstant.DatabaseConfig.USERNAME);
        String password = MapUtils.getString(databaseConfigMap, ConfigKeyConstant.DatabaseConfig.PASSWORD);

        Map<String, Object> jsonConfigMap = ConfigUtil.getJsonConfigMap(configMap)
        boolean camelNameConvert = MapUtils.getBoolean(jsonConfigMap, ConfigKeyConstant.ExtConfig.CAMEL_NAME_KEY, true);

        def sql = Sql.newInstance(url, username, password, driverName);
        try {
            TableModelVo tableModel = new TableModelVo();
            TableDefineVo tableDefine = new TableDefineVo();
            def columns = [];
            tableDefine.columns = columns;
            tableModel.tableDefine = tableDefine;

            def meta = sql.connection.metaData
            String[] schemaTypes = new String[1];
            schemaTypes[0] = "TABLE";
            def dataBaseName = getDataBaseName(meta)
            initTableInfoVo(sql, meta, tableDefine, tableModelPageVo, camelNameConvert)
            initTableColumn(sql, meta, tableDefine, camelNameConvert, dataBaseName, tableModelPageVo.getTableName(),)
            def pks = meta.getPrimaryKeys(dataBaseName, dataBaseName, tableModelPageVo.getTableName());
            while (pks.next()) {
                tableDefine.columns.forEach({
                    if (it.columnName.equalsIgnoreCase(pks.getString('COLUMN_NAME'))) {
                        it.isPK = true
                    }
                })
            }

            //将解析的数据设置回context
            tableModelContext.setTableModelVo(tableModel)
        } catch (Exception e) {
            LogHelper.EXCEPTION.error("获取数据库信息失败", e);
            throw DefaultExceptionMaker.buildException("获取数据库信息失败，原因是" + e.getMessage(), ResultCodeEnum.EXCEPTION);
        } finally {
            sql.close();
        }
    }


    /**
     * 获取数据库名称
     * @param meta
     * @return
     */
    private String getDataBaseName(DatabaseMetaData meta) {
        String database = null;
        try {
            String productName = meta.databaseProductName
            if (productName.equalsIgnoreCase("MYSQL")) {
                database = meta.database;
            } else if (productName.equalsIgnoreCase("Oracle")) {
                database = meta.getUserName();
            } else {
                LogHelper.EXCEPTION.error("数据库识别失败");
            }
        } catch (Exception e) {
            LogHelper.EXCEPTION.error("获取数据库名称失败！", e)
        }
        return database
    }

    private def initTableColumn(Sql sql, DatabaseMetaData meta, TableDefineVo tableDefine, Boolean camelNameConvert, String dataBaseName, String tableName) {
        try {
            TableNameUtil tableNameUtil = new TableNameUtil();
            String productName = meta.databaseProductName
            Set<String> columnSet = new HashSet<>()
            def cols;
            if (productName.equalsIgnoreCase("MYSQL")) {
                cols = meta.getColumns(dataBaseName, dataBaseName, tableName, null);
                while (cols.next()) {
                    ColumnVo column = new ColumnVo();
                    column.columnName = cols.getString('COLUMN_NAME');

                    //已经添加过了，不知道为什么会有重复信息
                    if (columnSet.contains(column.columnName)) {
                        continue;
                    }
                    column.dataName = column.columnName;
                    if (camelNameConvert) {
                        column.dataName = tableNameUtil.convertToBeanNames(column.columnName);
                    }
                    column.columnType = cols.getString('TYPE_NAME');
                    column.length = cols.getInt('COLUMN_SIZE');
                    column.isPK = false;
                    column.canBeNull = cols.getString('IS_NULLABLE') == "YES";
                    column.comment = cols.getString('REMARKS');
                    tableDefine.columns.add(column);
                    columnSet.add(column.columnName)
                }
            } else if (productName.equalsIgnoreCase("Oracle")) {
                String sqlcols = """
                    SELECT t.column_name                        AS column_name,
                           t.data_type                          AS type_name,
                           DECODE(t.data_precision, NULL,
                                  DECODE(t.data_type, 'CHAR', t.char_length, 'VARCHAR', t.char_length, 'VARCHAR2', t.char_length,
                                         'NVARCHAR2', t.char_length, 'NCHAR', t.char_length, 'NUMBER', 0, t.data_length), t.data_precision)
                                                                AS column_size,
                           DECODE(t.nullable, 'N', 'NO', 'YES') AS is_nullable,
                           t2.comments                          AS remarks
                      FROM user_tab_cols t
                               INNER JOIN user_col_comments t2 ON t2.table_name = t.table_name AND t2.column_name = t.column_name
                     WHERE t.table_name = ?
                     ORDER BY t.table_name, t.column_id
                """
                cols = sql.eachRow(sqlcols, [tableName], { row ->
                    ColumnVo column = new ColumnVo();
                    column.columnName = row[0];
                    if (!columnSet.contains(column.columnName)) {
                        column.dataName = column.columnName;
                        if (camelNameConvert) {
                            column.dataName = tableNameUtil.convertToBeanNames(column.columnName);
                        }
                        column.columnType = row[1];
                        column.length = row[2];
                        column.isPK = false;
                        column.canBeNull = row[3] == "YES";
                        column.comment = row[4];
                        tableDefine.columns.add(column);
                        columnSet.add(column.columnName)
                    }
                })
            } else {
                LogHelper.EXCEPTION.error("数据库识别失败");
            }
            return cols;
        } catch (Exception e) {
            LogHelper.EXCEPTION.error("字段相关信息获取失败, tableName={}", tableName, e);
        }
    }

    private void initTableInfoVo(Sql sql, DatabaseMetaData meta, TableDefineVo tableDefine, TableModelPageVo tableModelPageVo, Boolean camelNameConvert) {
        TableNameUtil tableNameUtil = new TableNameUtil()
        try {
            String productName = meta.databaseProductName
            if (productName.equalsIgnoreCase("MYSQL")) {
                String pageSql = """
                    SELECT table_name, table_comment FROM information_schema.tables
                    WHERE table_schema = ? and table_name = ?
                """;
                def row = sql.firstRow(pageSql, [meta['database'], tableModelPageVo.tableName])
                tableDefine.id = camelNameConvert ? tableNameUtil.upperFirst(tableNameUtil.convertToBeanNames(tableModelPageVo.tableName)) : row[0];
                tableDefine.dbTableName = row[0];
                if (StringUtils.isNotBlank(tableModelPageVo.getCnname())) {
                    tableDefine.cnname = tableModelPageVo.getCnname();
                } else {
                    tableDefine.cnname = row[1];
                    if (tableDefine.cnname == null || tableDefine.cnname == '') {
                        tableDefine.cnname = tableDefine.id;
                    }
                }
            } else if (productName.equalsIgnoreCase("Oracle")) {
                String pageSql = """
                       select table_name, comments from user_tab_comments where table_name = ?
                """
                def row = sql.firstRow(pageSql, [tableModelPageVo.tableName])
                tableDefine.id = camelNameConvert ? tableNameUtil.upperFirst(tableNameUtil.convertToBeanNames(tableModelPageVo.tableName)) : row[0];
                tableDefine.dbTableName = row[0];
                if (StringUtils.isNotBlank(tableModelPageVo.getCnname())) {
                    tableDefine.cnname = tableModelPageVo.getCnname();
                } else {
                    tableDefine.cnname = row[1];
                    if (tableDefine.cnname == null || tableDefine.cnname == '') {
                        tableDefine.cnname = tableDefine.id;
                    }
                }
            } else {
                LogHelper.EXCEPTION.error("数据库识别失败");
            }
        } catch (Exception e) {
            LogHelper.EXCEPTION.error("获取表注释失败", e);
        }
    }
}
