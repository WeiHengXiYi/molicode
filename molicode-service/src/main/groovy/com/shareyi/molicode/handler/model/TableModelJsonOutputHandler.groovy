package com.shareyi.molicode.handler.model

import com.alibaba.fastjson.JSON
import com.alibaba.fastjson.serializer.SerializerFeature
import com.google.common.collect.Lists
import com.google.common.collect.Sets
import com.shareyi.molicode.common.chain.handler.SimpleHandler
import com.shareyi.molicode.common.chain.handler.awares.TableModelHandlerAware
import com.shareyi.molicode.common.constants.CommonConstant
import com.shareyi.molicode.common.utils.FileIoUtil
import com.shareyi.molicode.common.utils.Profiles
import com.shareyi.molicode.common.utils.PubUtils
import com.shareyi.molicode.common.vo.code.ColumnVo
import com.shareyi.molicode.common.vo.code.CustomerCol
import com.shareyi.molicode.common.vo.code.CustomerTable
import com.shareyi.molicode.common.vo.code.TableDefineVo
import com.shareyi.molicode.common.vo.code.TableModelVo
import com.shareyi.molicode.common.vo.page.TableModelPageVo
import com.shareyi.molicode.context.TableModelContext
import org.apache.commons.io.FileUtils
import org.apache.commons.lang3.StringUtils
import org.springframework.stereotype.Service

import java.util.stream.Collectors

/**
 * tableModel 输出为json文件处理器
 *
 * @author david* @since 2018/10/7
 */
@Service
class TableModelJsonOutputHandler extends SimpleHandler<TableModelContext> implements TableModelHandlerAware {
    @Override
    int getOrder() {
        return 7;
    }

    @Override
    boolean shouldHandle(TableModelContext tableModelContext) {
        return Objects.equals(tableModelContext.tableModelPageVo.modelType, CommonConstant.MODEL_TYPE_JSON) &&
                !tableModelContext.isReadonly();
    }

    @Override
    void doHandle(TableModelContext tableModelContext) {
        TableModelVo tableModelVo = tableModelContext.tableModelVo;
        TableDefineVo tableDefineVo = tableModelVo.tableDefine;
        TableModelPageVo tableModelPageVo = tableModelContext.tableModelPageVo;
        File f = new File(FileIoUtil.contactPath(tableModelPageVo.tableModelDir, tableDefineVo.dbTableName?.trim() + ".json"));

        //如果文件已经存在，需要读取，并将已配置的信息配置回来
        if (f.exists()) {
            loadFromPreConfigInfo(f, tableModelVo);
        } else {
            loadInitCustomColInfo(tableModelVo)
        }


        FileIoUtil.makeSureFileExist(f);
        tableModelVo.clearCache(); //必须要清空
        //换行符号替换为Windows的换行符号
        f.withWriter("utf-8")
                { writer ->
                    writer.write JSON.toJSONString(tableModelVo, SerializerFeature.PrettyFormat, SerializerFeature.QuoteFieldNames, SerializerFeature.DisableCircularReferenceDetect);
                }

        print "table " + tableModelPageVo.tableName + "的json文件成功生成，在：" + f.absolutePath;
        tableModelContext.setOutputPath(f.absolutePath)
    }

    /**
     * 从之前的配置信息中获取配置信息
     *
     * @param file
     * @param tableModelVo
     */
    void loadFromPreConfigInfo(File file, TableModelVo tableModelVo) {
        String preConfig = FileUtils.readFileToString(file, Profiles.instance.fileEncoding);
        if (StringUtils.isBlank(preConfig)) {
            return;
        }
        List<String> allColumnList = Lists.newArrayList();
        Set<String> columnSet = Sets.newHashSet()
        tableModelVo.tableDefine.columns.each {
            column ->
                columnSet.add(column.getColumnName())
                allColumnList.add(column.getColumnName())
        }
        TableModelVo preTableModelVo = JSON.parseObject(preConfig, TableModelVo.class);
        preTableModelVo.clearCache(); //必须要清空

        tableModelVo.orderColumns = preTableModelVo.orderColumns;
        for (Map.Entry<String, String> entry : preTableModelVo.bizFieldsMap) {
            String key = entry.getKey();
            if (Objects.equals(key, "allColumn")) {
                continue;
            } else {
                String columnNames = this.filterColumn(entry.getValue(), columnSet)
                if (StringUtils.isNotEmpty(columnNames)) {
                    tableModelVo.bizFieldsMap.put(entry.key, columnNames);
                }
            }
        }
        tableModelVo.tableDefine.id = preTableModelVo.tableDefine.id;
        tableModelVo.tableDefine.cnname = preTableModelVo.tableDefine.cnname;
        tableModelVo.tableDefine.pageSize = preTableModelVo.tableDefine.pageSize;
        tableModelVo.tableDefine.isPaged = preTableModelVo.tableDefine.isPaged;

        Map<String, Object> tableMap = new HashMap<>();
        tableModelVo.customerTables.each {
            def value = preTableModelVo.tableDefine.myTableProp.get(it.key)
            if (value == null) {
                tableMap.put(it.key, it.defaultVal);
            } else {
                tableMap.put(it.key, value);
            }
        }
        tableModelVo.tableDefine.myTableProp = tableMap;

        List<ColumnVo> newCols = new ArrayList<>();
        //从json文件中恢复表数据
        preTableModelVo.tableDefine.columns.each { preColumn ->
            ColumnVo columnVo = tableModelVo.tableDefine.getColumnByColumnName(preColumn.columnName);
            if (columnVo != null) {
                // 数据库原生列
                columnVo.setCanBeNull(preColumn.canBeNull)
                columnVo.setReadonly(preColumn.readonly)
                columnVo.setCanBeNull(preColumn.canBeNull)
                columnVo.setIsPK(preColumn.isPK)
                columnVo.setIsCustomCol(false)
                Map<String, Object> map = new HashMap<>();
                tableModelVo.customerCols.each {
                    Object value = preColumn.myProp.get(it.key)
                    if (value == null) {
                        map.put(it.key, it.defaultVal)
                    } else {
                        map.put(it.key, value)
                    }
                };
                columnVo.myProp = map
                newCols.add(columnVo);
                tableModelVo.tableDefine.getColumnMap().remove(preColumn.columnName)
            } else {
                // 自定义列
                columnVo = new ColumnVo()
                columnVo.setDataName(preColumn.dataName)
                columnVo.setColumnName(preColumn.columnName)
                columnVo.setColumnType(preColumn.columnType)
                columnVo.setDictName(preColumn.dataName)
                columnVo.setComment(preColumn.comment)
                columnVo.setLength(preColumn.getLength().toString())

                columnVo.setCanBeNull(preColumn.canBeNull)
                columnVo.setReadonly(preColumn.readonly)
                columnVo.setCanBeNull(preColumn.canBeNull)
                columnVo.setIsPK(preColumn.isPK)
                columnVo.setIsCustomCol(true)
                Map<String, Object> map = new HashMap<>();
                tableModelVo.customerCols.each {
                    Object value = preColumn.myProp.get(it.key)
                    if (value == null) {
                        map.put(it.key, it.defaultVal)
                    } else {
                        map.put(it.key, value)
                    }
                };
                columnVo.myProp = map
                newCols.add(columnVo);
            }
        }
        //若数据库发生变化
        tableModelVo.tableDefine.getColumnMap().each {
            ColumnVo columnVo = it.value
            columnVo.setIsCustomCol(false)
            List<CustomerCol> cols = tableModelVo.customerCols
            Map<String, Object> colMap = new HashMap<>();
            cols.each { col ->
                colMap.put(col.key, col.defaultVal)
            }
            columnVo.myProp = colMap
            newCols.add(columnVo);
        }
        tableModelVo.tableDefine.columns = newCols;
    }

    /**
     * 过滤字段，必须在新的数据库也存在
     * @param columnNames
     * @param allColumnSet
     * @return
     */
    String filterColumn(String columnNames, HashSet<String> allColumnSet) {
        List<String> columnNameList = PubUtils.stringToList(columnNames);
        List<String> filteredList = Lists.newArrayList();
        columnNameList.each { name ->
            if (allColumnSet.contains(name)) {
                filteredList.add(name);
            }
        }
        return StringUtils.join(filteredList, ",");
    }

    /**
     * 表自定义属性初始化
     * @param tableModelVo
     */
    void loadInitCustomColInfo(TableModelVo tableModelVo) {
        List<CustomerTable> tables = tableModelVo.customerTables
        Map<String, Object> tableMap = new HashMap<>();
        tables.each { table ->
            tableMap.put(table.key, table.defaultVal)
        }
        tableModelVo.tableDefine.myTableProp = tableMap

        List<ColumnVo> columns = tableModelVo.tableDefine.columns
        columns.each { column ->
            List<CustomerCol> cols = tableModelVo.customerCols
            Map<String, Object> colMap = new HashMap<>();
            cols.each { col ->
                colMap.put(col.key, col.defaultVal)
            }
            column.myProp = colMap
        }
    }
}
