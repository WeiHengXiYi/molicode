INSERT
  INTO `auto-code`.ac_common_ext_info (id, owner_type, owner_code, ext_key, ext_value, type, creator, operator,
                                       concurrent_version, data_version, status, created, modified)
VALUES (1, 3, 'admin', 'defaultProjectKey', '12F31BD5E8D14A0A96D82ED24070D9A0', 1, null, 'admin', 12, 1, 1,
        '2019-08-16 09:27:16', '2019-08-16 09:27:16');
INSERT
  INTO `auto-code`.ac_config (id, type, project_key, scope, config_key, config_value, ext1, ext2, ext3, creator,
                              operator, concurrent_version, data_version, status, created, modified)
VALUES (16, 2, '12F31BD5E8D14A0A96D82ED24070D9A0', 1, 'pathConfig',
        '{"outputType":"1","templateBaseDir":"E:\\\\SPACE_TEMPLE\\\\baic-template\\\\autocode-baic","projectOutputDir":"E:\\\\outdir","templateType":"local","gitUrl":"","branchName":"master","templateRelativePath":"","userName":"","password":""}',
        null, null, null, 'admin', 'admin', 9, 1, 1, '2021-04-28 07:59:50', '2021-04-28 07:59:50'),
       (17, 2, '12F31BD5E8D14A0A96D82ED24070D9A0', 1, 'databaseConfig',
        '{"databaseName":"mysql","driverClass":"com.mysql.jdbc.Driver","url":"jdbc:mysql://127.0.0.1:3306/auto-code?useUnicode=true&characterEncoding=UTF8&serverTimezone=UTC","username":"root","password":"123456"}',
        null, null, null, 'admin', 'admin', 7, 1, 1, '2021-04-28 08:00:23', '2021-04-28 08:00:23'),
       (18, 2, '12F31BD5E8D14A0A96D82ED24070D9A0', 1, 'codeConfig',
        '{"author":"lyz","basePackage":"com.yonyou.cms.wholesales.service","artifactId":"wholesales","category":""}',
        null, null, null, 'admin', 'admin', 2, 1, 1, '2021-04-28 08:01:13', '2021-04-28 08:01:13');

INSERT
  INTO `auto-code`.ac_project (id, name, remark, project_key, type, ext1, ext2, ext3, creator, operator,
                               concurrent_version, data_version, status, created, modified)
VALUES (6, '项目代码生成', '', '12F31BD5E8D14A0A96D82ED24070D9A0', 2, '', '', '', 'admin', 'admin', 4, 1, 1,
        '2021-04-28 07:59:16', '2021-04-28 07:59:16');

INSERT
  INTO `auto-code`.ac_user (id, user_name, nick_name, gender, password_md5, birth_day, user_mark, role_code, remark,
                            ext1, ext2, ext3, creator, operator, concurrent_version, data_version, status, created,
                            modified)
VALUES (1, 'admin', '超级管理员', 1, '96825a5d8656da754aeab9fecb9fdba8', '1990-01-01', null, 'sys_admin', '默认账号', null, null,
        null, null, null, 1, 1, 1, '2019-07-01 00:00:00', '2019-07-01 00:00:00');
