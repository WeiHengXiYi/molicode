package com.shareyi.molicode.configuaration;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Configuration;

/**
 * springboot启动入口类
 *
 * @author david
 * @date 2018-09-23
 */
@Slf4j
@Configuration
public class AfterStartedConfiguration implements CommandLineRunner {

    @Value("${server.port}")
    private String port;

    @Override
    public void run(String... args) throws Exception {
        log.info("http://127.0.0.1:{}/index.html?t={}", port, System.currentTimeMillis());
    }

}
