package com.shareyi.molicode;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

/**
 * springboot启动入口类
 *
 * @author david
 * @date 2018-09-23
 */
@EnableConfigurationProperties
@MapperScan(basePackages = "com.shareyi.molicode.dao")
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
public class SpringbootApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootApplication.class);
    }

}
